
function generateXMI (t, stage, containments, classes_links, selection){
  return new Promise(function (resolve, reject){
    var arrayEl = [];
    var builder = require('xmlbuilder');
    var stringdiagram = '{ "'+selection.ecore_name+':'+selection.diagram_name+'": { "@xmi:version": "2.0", "@xmlns:xmi": "http://www.omg.org/XMI", "@xmlns:'+selection.ecore_name+'": "http://www.example.org/'+selection.ecore_name+'"}}';
    var diagram = JSON.parse(stringdiagram);
    
    stage.children[0].children.forEach(element => {
      if (element.attrs.data != undefined){
        arrayEl.push(element);
      }
    });
    
    var root = builder.create(diagram, { encoding: 'UTF-8' });
    stage.children[0].children.forEach(element => {
      if (element.attrs.data != undefined){
        var item = "";
        containments.forEach(containment => {
          if (containment.eType == "#//"+element.attrs.data.eType || containment.eType == element.attrs.data.eSuperTypes){
            item = root.ele(containment.name);
            if (element.attrs.data.eSuperTypes != undefined){
              item.att("xsi:type", t.selection.ecore_name+":"+element.attrs.data.eType);
            }
            element.attrs.data.attrs.forEach(att =>{
              item.att(att.name, att.value);
            });

            element.attrs.data.comps.forEach(comp =>{
              var compartiment = item.ele(comp.name);
              if (comp.eSuperTypes != undefined){
                compartiment.att("xsi:type", t.selection.ecore_name+":"+comp.eName);
              }
              stage.children[0].children.forEach(group =>{
                var i= 0;
                group.children.forEach (children =>{
                  if (i>=4){
                    if (children._id == comp.target_id){
                      children.attrs.data.attrs.forEach(att=>{
                        compartiment.att(att.name, att.value);
                      })
                      generateRecursive (t, children, classes_links, compartiment, arrayEl, stage)
                    }
                    
                  }
                  
                  i++;
                })
                

              })
              
            });
            
            var names = [];
            // element.attrs.data.refs.forEach(ref =>{
            //   var c = 0;
            //   arrayEl.forEach(el=>{
            //     if (el._id == ref.target_id){
            //       item.att(ref.name, ref.value+"."+c);
            //     }
            //     if (ref.eSuperTypes == el.attrs.data.eSuperTypes || ref.eType == el.attrs.data.eType){
            //       c++;
            //     }
            //   })
            // });
            element.attrs.data.refs.forEach(ref =>{
              nodes = [];cnodes = [];
              FindInTree(stage.children[0], ref.target_id, t);
              var elem = ""
              cnodes.forEach(node => {
                elem=elem+node
              })
                if (item.attribs[ref.name] != undefined){
                  elem = item.attribs[ref.name].value +" "+ elem;
                }
            
                item.att(ref.name.replace("//", "/"), elem);
            });
            
          }
        })
      }
    });

    classes_links.forEach(element =>{
      var item = "";
      containments.forEach(containment => {
         if (containment.eType == "#//"+element.eType || containment.eType == element.eSuperTypes){
           item = root.ele(containment.name);
           if (element.eSuperTypes != undefined){
             item.att("xsi:type", t.selection.ecore_name+":"+element.eType);
           }
           element.attrs.forEach(att =>{
             item.att(att.name, att.value);
           });
      
           var names = [];
           element.refs.forEach(ref =>{
             var c = 0;
             arrayEl.forEach(el=>{
               if (el._id == element.source_id){
                 console.log(item)
                 item.att(ref.source.name, ref.value+"."+c);
               }
               if (ref.source.eSuperTypes == el.attrs.data.eSuperTypes || ref.source.eType == el.attrs.data.eType){
                 c++;
               }
             })
             var c = 0;
             arrayEl.forEach(el=>{
               if (el._id == element.target_id){
                console.log(item)
                 item.att(ref.target.name, ref.value+"."+c);
               }
               if (ref.target.eSuperTypes == el.attrs.data.eSuperTypes || ref.target.eType == el.attrs.data.eType){
                 c++;
               }
             })
           });
         }
   });
   });
   
    var xmi= root.end({ pretty: true});
    resolve(xmi);

  });
}


var nodes = [];
var cnodes = [];
function FindInTree(element, target_id, t){
  if (nodes.length == 0){
    nodes.push("/")
  }else{
    var value=element._id;
    if (element.attrs != undefined){
    var copchil = "";
    element.parent.children.forEach(chil => {
      if (chil._id == element._id){
        copchil= chil;
      }
    })
    if (copchil.attrs.data != undefined){
      t.containments.forEach(cont =>{
        if (cont.eType == "#//"+copchil.attrs.data.eType || cont.eType == copchil.attrs.data.eSuperTypes){
          value =  "/@"+cont.name;
        }
      })
      t.pallete.compartments.forEach(comp => {
        if (comp.eType == "#//"+copchil.attrs.data.eType || comp.eType == copchil.attrs.data.eSuperTypes){
          value =  "/@"+comp.name;
        }
      })
      var c = 0;
      element.parent.children.forEach(chil => {
        if (chil._id == copchil._id){
          value =value+"."+c
        }
        if(chil.attrs.data != undefined && copchil.attrs.data.eType == chil.attrs.data.eType) {
          c++ ;
        }
      })
    }
  }
    nodes.push(value)
  }
  
  if (element._id == target_id){
    nodes.forEach(node => {
      cnodes.push(node);
    })
    return true;
  }
  
  element.children.forEach(chil =>{
    if (FindInTree(chil, target_id, t))
      return true;
    
    nodes.pop();
    return false;
  })
   
  
 
}


function generateRecursive (t, element, classes_links, root, arrayEl, stage){
  
  element.children.forEach(element => {
    if (element.attrs.data != undefined){
      var item = "";
      t.pallete.compartments.forEach(containment => {
        if (containment.eType == "#//"+element.attrs.data.eType || containment.eType == element.attrs.data.eSuperTypes){
          item = root.ele(containment.name);
          if (element.attrs.data.eSuperTypes != undefined){
            item.att("xsi:type", t.selection.ecore_name+":"+element.attrs.data.eType);
          }
          element.attrs.data.attrs.forEach(att =>{
            item.att(att.name, att.value);
          });

          element.attrs.data.comps.forEach(comp =>{
            var compartiment = item.ele(comp.name);
            if (comp.eSuperTypes != undefined){
              compartiment.att("xsi:type", t.selection.ecore_name+":"+comp.eName);
            }
            
            element.parent.children.forEach(group =>{
              var i= 0; var attadded = false;
              group.children.forEach (children =>{
                if (i>=4){
                  if (children._id == comp.target_id){
                    children.attrs.data.attrs.forEach(att=>{
                      compartiment.att(att.name, att.value);
                    })
                    generateRecursive (t, children, classes_links, compartiment, arrayEl, stage)
                  } 
                }
                i++;
              })
              

            })
            
          });
     
          var names = [];
          element.attrs.data.refs.forEach(ref =>{
              nodes = [];cnodes = [];
              FindInTree(stage.children[0], ref.target_id, t);
              var elem = ""
              cnodes.forEach(node => {
                elem=elem+node
              })
              if (item.attribs[ref.name] != undefined){
                elem = item.attribs[ref.name].value +" "+ elem;
              }
          
              item.att(ref.name.replace("//", "/"), elem);
            });
          
        }
      })
    }
  });

  classes_links.forEach(element =>{
    var item = "";
    t.pallete.compartments.forEach(containment => {
       if (containment.eType == "#//"+element.eType || containment.eType == element.eSuperTypes){
         item = root.ele(containment.name);
         if (element.eSuperTypes != undefined){
           item.att("xsi:type", t.selection.ecore_name+":"+element.eType);
         }
         element.attrs.forEach(att =>{
           item.att(att.name, att.value);
         });
    
         var names = [];
         element.refs.forEach(ref =>{
          nodes = [];cnodes = [];
          FindInTree(stage.children[0], ref.target_id, t);
          var elem = ""
          cnodes.forEach(node => {
            elem=elem+node
          })
          item.att(ref.source.name.replace("//", "/"), elem);
          nodes = [];cnodes = [];
          FindInTree(stage.children[0], ref.target_id, t);
          var elem = ""
          cnodes.forEach(node => {
            elem=elem+node
          })
          item.att(ref.target.name.replace("//", "/"), elem);
         });
       }
 });
 });
}

export {
    generateXMI
}