import Konva from "konva";

function loadFromFile (file, t){

    file = JSON.parse(file)
    
    file.classes_links.forEach(link =>{
      t.classes_links.push(link);
    })

    file.groups.forEach(lg => {
      var loadedgroup = JSON.parse(lg.group)
        if (loadedgroup.attrs.data.grouptype == "node"){
            addNode(loadedgroup, lg, t)
            
        }
        if (loadedgroup.attrs.data.grouptype == "arrow"){
          addArrow (loadedgroup, lg, t)
         
        }

       
        t.stage.draw();
    })
}

function addNode(loadedgroup, lg, t){
  var group = new Konva.Group({
    x: loadedgroup.attrs.x, y: loadedgroup.attrs.y, height: loadedgroup.attrs.height, width: loadedgroup.attrs.width, visible: loadedgroup.attrs.visible,
    draggable: true, id: loadedgroup.attrs.id, data: loadedgroup.attrs.data
});
group._id = lg._id;
var loadedfigure = loadedgroup.children[0];
    var figure;
    if  (loadedfigure.className=="Circle"){
        figure = new Konva.Circle({
            x: loadedfigure.attrs.x,  y: loadedfigure.attrs.y, scaleX: loadedfigure.attrs.scaleX, scaleY: loadedfigure.attrs.scaleY, radius: loadedfigure.attrs.radius, fill: loadedfigure.attrs.fill, 
            stroke: loadedfigure.attrs.stroke, strokeWidth: loadedfigure.attrs.strokeWidth, dash : loadedfigure.attrs.dash
        })
    }
    if  (loadedfigure.className=="Rect"){
        figure = new Konva.Rect({
            x: loadedfigure.attrs.x,  y: loadedfigure.attrs.y, scaleX: loadedfigure.attrs.scaleX, scaleY: loadedfigure.attrs.scaleY, height: loadedfigure.attrs.height, width: loadedfigure.attrs.width, resizable : true,
            fill: loadedfigure.attrs.fill,
            stroke: loadedfigure.attrs.stroke, strokeWidth: loadedfigure.attrs.strokeWidth, dash : loadedfigure.attrs.dash
          });
    }
    if  (loadedfigure.className=="Image"){
        const svg =t.svgmap.get(t.pallete.nodes[loadedgroup.attrs.data.index].svg_uri);
        const blob = new Blob([svg], {type: 'image/svg+xml'});
        const url = URL.createObjectURL(blob);
        var imageObj = new Image();
        imageObj.src =  url;
        imageObj.onload = function() {
        figure = new Konva.Image({
          x: loadedfigure.attrs.x,
          y: loadedfigure.attrs.y,
          scaleX: loadedfigure.attrs.scaleX, scaleY: loadedfigure.attrs.scaleY,
          image: imageObj,
          width: loadedfigure.attrs.width,
          height: loadedfigure.attrs.height,
          data: loadedfigure.attrs.data,
          stroke: loadedfigure.attrs.stroke, strokeWidth: loadedfigure.attrs.strokeWidth, dash : loadedfigure.attrs.dash
        });
        var tr1 = new Konva.Transformer({ visible: false, node: figure, rotateEnabled: false, enabledAnchors: ['bottom-right'],  boundBoxFunc: function(oldBoundBox, newBoundBox) {
            if (Math.abs(newBoundBox.width) < MIN_WIDTH) {
              return oldBoundBox;
            }return newBoundBox;}});
            t.tranformations.push(tr1);
            figure.on('transformend', function() {
              t.$store.dispatch('updateTexts', group)
              t.$store.dispatch('updateArrows', group)
            });
        group.add(figure);group.add(tr1);
        t.stage.draw();
        }
    }
    var MIN_WIDTH = 100;
    if  (loadedfigure.className=="Rect"){
        var tr1 = new Konva.Transformer({ visible: false, node: figure, rotateEnabled: false, enabledAnchors: ['bottom-right'],  boundBoxFunc: function(oldBoundBox, newBoundBox) {
            if (Math.abs(newBoundBox.width) < MIN_WIDTH) {
              return oldBoundBox;
            }return newBoundBox;}});
            t.tranformations.push(tr1);
            figure.on('transformend', function() {
              t.$store.dispatch('updateTexts', group)
              t.$store.dispatch('updateArrows', group)
            });
        group.add(figure);group.add(tr1);
        var loadednameText= loadedgroup.children[2];
        var nameText = new Konva.Text({
            y:40, x:0, width: loadednameText.attrs.width, height: 50,
            text: loadednameText.attrs.text, align: "center",
            fontSize: 14, fontFamily: 'Calibri',  fill: 'black'
        });
        var loadedlabelText= loadedgroup.children[3];
        var labelText = new Konva.Text({
        y: 55, x:0, width: loadedlabelText.attrs.width, height: 35,
        text: loadedlabelText.attrs.text, align: "center",
        fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
        data : loadedlabelText.attrs.data
        });
        group.add(nameText); group.add(labelText);
    }  
    if  (loadedfigure.className=="Circle"){
      var tr1 = new Konva.Transformer({ visible: false, node: figure, rotateEnabled: false, enabledAnchors: ['bottom-right'],  boundBoxFunc: function(oldBoundBox, newBoundBox) {
          if (Math.abs(newBoundBox.width) < MIN_WIDTH) {
            return oldBoundBox;
          }return newBoundBox;}});
          t.tranformations.push(tr1);
          figure.on('transformend', function() {
            t.$store.dispatch('updateTexts', group)
            t.$store.dispatch('updateArrows', group)
          });
      group.add(figure);group.add(tr1);
      var loadednameText= loadedgroup.children[2];
      var nameText = new Konva.Text({
        y:-10, x:-50, width: loadednameText.attrs.width, height: 50,
        text: loadednameText.attrs.text, align: "center",
        fontSize: 14, fontFamily: 'Calibri',  fill: 'black'
      });
      var loadedlabelText= loadedgroup.children[3];
      var labelText = new Konva.Text({
        y: 5, x:-50, width: loadedlabelText.attrs.width, height: 40,
        text: loadedlabelText.attrs.text, align: "center",
        fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
        data : loadedlabelText.attrs.data
      });
      group.add(nameText); group.add(labelText);
  }  

    group.on('mouseover', function() {
        if (!t.arrowData.enable)
          document.body.style.cursor = 'pointer';
      });
      group.on('mouseout', function() {
        if (!t.arrowData.enable)
          document.body.style.cursor = 'default';
      });
  
      group.on('dragmove', function (){
        t.$store.dispatch('updateArrows', group)
      });
      group.on('dragstart', function() {
          //this.moveToTop();
          group.draw();
      });
  
      group.on('click', function(){
        if (!t.arrowData.enable && !t.arrowComp.enable){
          t.$modal.show('editor', {index: loadedgroup.attrs.data.index, group: group});
        }else if(t.arrowData.enable){
          if (t.arrowData.source == undefined){
            t.arrowData.source = group;
            //t.$store.dispatch('hideNodes',t.arrowData.targeteType)
          }else{
            if(group.attrs.data.comps.length==0){
              t.arrowData.target = group;
              t.drawArrow();
            }
          }
        }else{
           if (t.arrowComp.target == undefined){
            t.arrowComp.source = group;
            t.drawComp(t.arrowComp.pallete_index);
            t.saveCompInfo();
          }
        }
      });

      if (loadedgroup.children.length >4){
       var i = 0;
       var totalcomps = 0;
        loadedgroup.children.forEach(compgroup =>{
          if (i >= 4){
            addComp(compgroup, totalcomps, group, t)
          }
          i++;
        })

      }
      t.stage.children[0].add(group);
}

function addArrow (loadedgroup, lg, t) {
  var group = new Konva.Group({
    x: loadedgroup.attrs.x, y: loadedgroup.attrs.y, height: 100, width: 100, visible: true, data : loadedgroup.attrs.data
  });
  group._id = lg._id;
  var data = null;
  var labelText = null;
  
  var loadedarrow = loadedgroup.children[0];
  var arrow = new Konva.Arrow({
  points: loadedarrow.attrs.points,
  pointerLength: 20, pointerWidth: 20,
  fill: loadedarrow.attrs.fill,
  stroke: loadedarrow.attrs.stroke,
  strokeWidth: loadedarrow.attrs.strokeWidth,
  dash: loadedarrow.attrs.dash,
  data : loadedarrow.attrs.data
  });
 
  var labelText = null;
  if (loadedgroup.children.length == 2){
    var loadedlabelText = loadedgroup.children[1];
    labelText = new Konva.Text({
      x:loadedlabelText.attrs.x, y:  loadedlabelText.attrs.y, width:48, height:48, 
    text: loadedlabelText.attrs.text, align: "center",
    fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
    data :loadedlabelText.attrs.data
     });
  }
  

  t.$store.dispatch('addArrow', arrow);
  var data = null;
  
      t.classes_links.forEach(link =>{
        if (link.source_id == loadedgroup.children[0].attrs.data.source_id && link.target_id == loadedgroup.children[0].attrs.data.target_id){
          data = link;
        }
      });
 

  group.add(arrow)
  arrow.on('click', function(){
    t.showArrowDialog(arrow, data, group)
  });
  group.on('mouseover', function() {
    if (!t.arrowData.enable)
      document.body.style.cursor = 'pointer';
  });
  group.on('mouseout', function() {
    if (!t.arrowData.enable)
      document.body.style.cursor = 'default';
  });
  if (labelText!=null)
    group.add(labelText)
  t.stage.children[0].add(group);
  t.stage.draw();
  t.resert();

}

function addComp (compgroup, totalcomps, group, t) {
  if (compgroup.attrs.data.grouptype == "comp"){
    var comp = new Konva.Group({
        x: compgroup.attrs.x, y: compgroup.attrs.y, height: compgroup.attrs.height, width: compgroup.attrs.width, visible: compgroup.attrs.visible,
        draggable: true, id: compgroup.attrs.id, dragBoundFunc: function(pos) {

          var r = (group.children[0].attrs.width)/2;  
          if (group.children[0].attrs.radius != undefined)
            r = (group.children[0].attrs.radius);

          var x = group.parent.absolutePosition().x -r  + (group.parent.children[0].attrs.width*group.parent.children[0].attrs.scaleX)/2 ;
          var y = group.parent.absolutePosition().y -r +(group.parent.children[0].attrs.height*group.parent.children[0].attrs.scaleY)/2 ;
          var radius = (group.parent.children[0].attrs.height*group.parent.children[0].attrs.scaleY)/2;
          if (group.parent.children[0].attrs.radius != undefined){
            x = group.parent.absolutePosition().x  -r + (group.parent.children[0].attrs.radius*group.parent.children[0].attrs.scaleX)  ;
            y = group.parent.absolutePosition().y  -r +(group.parent.children[0].attrs.radius*group.parent.children[0].attrs.scaleX) ;
            radius = group.parent.children[0].attrs.radius* group.parent.children[0].attrs.scaleX;
          }
          var scale = radius / Math.sqrt(Math.pow(pos.x - x, 2) + Math.pow(pos.y - y, 2));
          if (scale < 1)
            return {
              y: Math.round((pos.y - y) * scale + y),
              x: Math.round((pos.x - x) * scale + x)
            };
          else return pos;
          
        },
        data: compgroup.attrs.data
    });
    comp._id = compgroup.attrs.data.copy_id;
    var loadedfigure = compgroup.children[0];
        var figure;
        if  (loadedfigure.className=="Circle"){
            figure = new Konva.Circle({
                x: loadedfigure.attrs.x,  y: loadedfigure.attrs.y, scaleX: loadedfigure.attrs.scaleX, scaleY: loadedfigure.attrs.scaleY, radius: loadedfigure.attrs.radius, fill: loadedfigure.attrs.fill, 
                stroke: loadedfigure.attrs.stroke, strokeWidth: loadedfigure.attrs.strokeWidth, dash : loadedfigure.attrs.dash
            })
        }
        if  (loadedfigure.className=="Rect"){
            figure = new Konva.Rect({
                x: loadedfigure.attrs.x,  y: loadedfigure.attrs.y, scaleX: loadedfigure.attrs.scaleX, scaleY: loadedfigure.attrs.scaleY, height: loadedfigure.attrs.height, width: loadedfigure.attrs.width, resizable : true,
                fill: loadedfigure.attrs.fill,
                stroke: loadedfigure.attrs.stroke, strokeWidth: loadedfigure.attrs.strokeWidth, dash : loadedfigure.attrs.dash
              });
        }
        if  (loadedfigure.className=="Image"){
            const svg =t.svgmap.get(t.pallete.nodes[compgroup.attrs.data.index].svg_uri);
            const blob = new Blob([svg], {type: 'image/svg+xml'});
            const url = URL.createObjectURL(blob);
            var imageObj = new Image();
            imageObj.src =  url;
            imageObj.onload = function() {
            figure = new Konva.Image({
              x: loadedfigure.attrs.x,
              y: loadedfigure.attrs.y,
              scaleX: loadedfigure.attrs.scaleX, scaleY: loadedfigure.attrs.scaleY,
              image: imageObj,
              width: loadedfigure.attrs.width,
              height: loadedfigure.attrs.height,
              data: loadedfigure.attrs.data,
              stroke: loadedfigure.attrs.stroke, strokeWidth: loadedfigure.attrs.strokeWidth, dash : loadedfigure.attrs.dash
            });
            var tr1 = new Konva.Transformer({ visible: false, node: figure, rotateEnabled: false, enabledAnchors: ['bottom-right'],  boundBoxFunc: function(oldBoundBox, newBoundBox) {
                if (Math.abs(newBoundBox.width) < MIN_WIDTH) {
                  return oldBoundBox;
                }return newBoundBox;}});
                t.tranformations.push(tr1);
                figure.on('transformend', function() {
                  t.$store.dispatch('updateTexts', comp)
                  t.$store.dispatch('updateArrows', comp)
                });
            comp.add(figure);comp.add(tr1);
            t.stage.draw();
            }
        }
        var MIN_WIDTH = 100;
        if  (loadedfigure.className=="Rect"){
          var tr1 = new Konva.Transformer({ visible: false, node: figure, rotateEnabled: false, enabledAnchors: ['bottom-right'],  boundBoxFunc: function(oldBoundBox, newBoundBox) {
              if (Math.abs(newBoundBox.width) < MIN_WIDTH) {
                return oldBoundBox;
              }return newBoundBox;}});
              t.tranformations.push(tr1);
              figure.on('transformend', function() {
                t.$store.dispatch('updateTexts', comp)
                t.$store.dispatch('updateArrows', comp)
              });
          comp.add(figure);comp.add(tr1);
          var loadednameText= compgroup.children[2];
          var nameText = new Konva.Text({
              y:40, x:0, width: loadednameText.attrs.width, height: 50,
              text: loadednameText.attrs.text, align: "center",
              fontSize: 14, fontFamily: 'Calibri',  fill: 'black'
          });
          var loadedlabelText= compgroup.children[3];
          var labelText = new Konva.Text({
          y: 55, x:0, width: loadedlabelText.attrs.width, height: 35,
          text: loadedlabelText.attrs.text, align: "center",
          fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
          data : loadedlabelText.attrs.data
          });
          comp.add(nameText); comp.add(labelText);
      }  
      if  (loadedfigure.className=="Circle"){
        var tr1 = new Konva.Transformer({ visible: false, node: figure, rotateEnabled: false, enabledAnchors: ['bottom-right'],  boundBoxFunc: function(oldBoundBox, newBoundBox) {
            if (Math.abs(newBoundBox.width) < MIN_WIDTH) {
              return oldBoundBox;
            }return newBoundBox;}});
            t.tranformations.push(tr1);
            figure.on('transformend', function() {
              t.$store.dispatch('updateTexts', comp)
              t.$store.dispatch('updateArrows', comp)
            });
        comp.add(figure);comp.add(tr1);
        var loadednameText= compgroup.children[2];
        var nameText = new Konva.Text({
          y:-10, x:-50, width: loadednameText.attrs.width, height: 50,
          text: loadednameText.attrs.text, align: "center",
          fontSize: 14, fontFamily: 'Calibri',  fill: 'black'
        });
        var loadedlabelText= compgroup.children[3];
        var labelText = new Konva.Text({
          y: 5, x:-50, width: loadedlabelText.attrs.width, height: 40,
          text: loadedlabelText.attrs.text, align: "center",
          fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
          data : loadedlabelText.attrs.data
        });
        comp.add(nameText); comp.add(labelText);
    }    

    comp.on('mouseover', function() {
            if (!t.arrowData.enable)
              document.body.style.cursor = 'pointer';
          });
          comp.on('mouseout', function() {
            if (!t.arrowData.enable)
              document.body.style.cursor = 'default';
          });
      
          comp.on('dragmove', function (){
            t.$store.dispatch('updateArrows', comp)
          });
          comp.on('dragstart', function() {
              //this.moveToTop();
              comp.draw();
          });
      
          comp.on('click', function(){
            console.log("comp click")
            if (!t.arrowData.enable && !t.arrowComp.enable){
              t.$modal.show('editor', {index: t.arrowComp.pallete_index, group: comp});
            }else if(t.arrowData.enable){
              if (t.arrowData.source == undefined){
                t.arrowData.source = comp;
              }else{
                if(comp.attrs.data.comps.length==0){
                  t.arrowData.target = comp;
                  t.drawArrow();
                }
              }
            }else if(t.arrowComp.enable){ 
               if (t.arrowComp.target == undefined){ 
                t.arrowComp.source = comp;
                t.drawComp(t.arrowComp.pallete_index);
                t.saveCompInfo();
              }
            }
          });
          
    group.add(comp);
    group.attrs.data.comps[totalcomps].target_id=comp._id;
    totalcomps++;
    if (compgroup.children.length >4){
      var i = 0;
      var totalcomps = 0;
      compgroup.children.forEach(compgroup2 =>{
       
         if (i >= 4){
           addComp(compgroup2, totalcomps, comp, t)
         }
         i++;
       })

     }
  }
}

export {
    loadFromFile
}