function drawComp(t, index){
  console.log("DRAW")
  var color ="";
    var trvisible = t.trShow;
    var dash ="";
    var border_width = "2";
    var border_color = ""
    if(t.pallete.nodes[t.arrowComp.pallete_index].color != undefined){
      color = t.pallete.nodes[t.arrowComp.pallete_index].color;
    }else{
      color = "255,255,255";
    }
    if(t.pallete.nodes[t.arrowComp.pallete_index].border_style == "dash"){
      dash = [10, 5];
    }else if(t.pallete.nodes[t.arrowComp.pallete_index].border_style == "dot"){
      dash = [1, 2];
    }else{
      dash = "";
    }

    if(t.pallete.nodes[t.arrowComp.pallete_index].border_color != undefined){
      border_color = t.pallete.nodes[t.arrowComp.pallete_index].border_color;
    }else{
      if (t.pallete.nodes[t.arrowComp.pallete_index].figure != "svg")
        border_color = "0,0,0";
      else
        border_color = "255,255,255";
    }
    if(t.pallete.nodes[t.arrowComp.pallete_index].border_width != undefined){
      border_width = t.pallete.nodes[t.arrowComp.pallete_index].border_width;
    }
    


    var comps = color.split(",");
    var comps2 = border_color.split(",");
    var item = null;
    var group = new Konva.Group({
        x: 10, y: 10, height: 0, width: 0, visible: true,
        draggable: true, dragBoundFunc: function(pos) {
          
          var r = (group.parent.children[0].attrs.width)/2;  
          if (group.parent.children[0].attrs.radius != undefined)
            r = (group.parent.children[0].attrs.radius);
            
            var x = group.parent.absolutePosition().x -r  + (group.parent.children[0].attrs.width*group.parent.children[0].attrs.scaleX)/2 ;
            var y = group.parent.absolutePosition().y -r +(group.parent.children[0].attrs.height*group.parent.children[0].attrs.scaleY)/2 ;
            var radius = (group.parent.children[0].attrs.height*group.parent.children[0].attrs.scaleY)/2;
         
          if (group.parent.children[0].attrs.radius != undefined){
            x = group.parent.absolutePosition().x  -r + (group.parent.children[0].attrs.radius*group.parent.children[0].attrs.scaleX)  ;
            y = group.parent.absolutePosition().y  -r +(group.parent.children[0].attrs.radius*group.parent.children[0].attrs.scaleX) ;
            radius = group.parent.children[0].attrs.radius* group.parent.children[0].attrs.scaleX;
          }
         
          var scale = radius / Math.sqrt(Math.pow(pos.x - x, 2) + Math.pow(pos.y - y, 2));
          if (scale < 1)
            return {
              y: Math.round((pos.y - y) * scale + y),
              x: Math.round((pos.x - x) * scale + x)
            };
          else return pos;
          
        }, 
        id: t.numelements, data: {grouptype : "comp", attrs:[], refs:[], comps: [], index: index,
        eType: t.pallete.nodes[t.arrowComp.pallete_index].name, eSuperTypes: t.pallete.nodes[t.arrowComp.pallete_index].eSuperTypes}
    });
    
    var node =  t.$store.state.pallete.nodes[t.arrowComp.pallete_index];
    group.attrs.data.attrs.numid = t.numelements;
      if (group.attrs.data.attrs.length == 0){
      if (node.eSuperTypes != undefined){
        t.$store.state.pallete.nodes.forEach(element => {
          if ("#//"+element.name == node.eSuperTypes){
            element.attrs.forEach(att=>{
              group.attrs.data.attrs.push({
                name : att.name,
                value : ''
              });
            })
          }
        });
      }else{
        node.attrs.forEach(att =>{
          group.attrs.data.attrs.push({
                name : att.name,
                value : ''
              });
        })
      }
    }
    var MIN_WIDTH = 100;
    t.$store.dispatch('incrementNumElements');
    switch (t.pallete.nodes[t.arrowComp.pallete_index].figure){
      case "svg":
        const svg =t.svgmap.get(t.pallete.nodes[index].svg_uri);
        const blob = new Blob([svg], {type: 'image/svg+xml'});
        const url = URL.createObjectURL(blob);
        var imageObj = new Image();
        imageObj.src =  url;
        imageObj.onload = function() {
        var image = new Konva.Image({
          x: 0,
          y: 0,
          image: imageObj,
          width: 100,
          height: 100,
          stroke: t.rgbToHex(comps2[0], comps2[1], comps2[2]), strokeWidth: Number(border_width),  dash :dash
        });
        var tr1 = new Konva.Transformer({ visible: trvisible, node: image, rotateEnabled: false, enabledAnchors: ['bottom-right'],  boundBoxFunc: function(oldBoundBox, newBoundBox) {
        if ((newBoundBox.width + group.attrs.x) > (group.parent.children[0].attrs.width*group.parent.children[0].attrs.scaleX) || (newBoundBox.height + group.attrs.y) > (group.parent.children[0].attrs.height*group.parent.children[0].attrs.scaleY)){
          return oldBoundBox;
        }
        if (Math.abs(newBoundBox.width) < MIN_WIDTH) {
          return oldBoundBox;
        }return newBoundBox;}});
        t.tranformations.push(tr1);
        image.on('transformend', function() {
          t.$store.dispatch('updateTexts', group);
          t.$store.dispatch('updateArrows', group)
        });
        group.add(image);group.add(tr1);
        t.stage.draw();
        };

      break;

      case "rounded":
        item = new Konva.Circle({
          x: 0,  y: 0, radius: 50,
          fill: t.rgbToHex(comps[0], comps[1], comps[2]),
          stroke: t.rgbToHex(comps2[0], comps2[1], comps2[2]), strokeWidth: Number(border_width),  dash :dash
        });
        var tr1 = new Konva.Transformer({ visible: trvisible, node: item, rotateEnabled: false, enabledAnchors: ['bottom-right'],  boundBoxFunc: function(oldBoundBox, newBoundBox) {
        if (Math.abs(newBoundBox.width) < MIN_WIDTH) {
          return oldBoundBox;
        }return newBoundBox;}});
        t.tranformations.push(tr1);
        item.on('transformend', function() {
          t.$store.dispatch('updateTexts', group);
          t.$store.dispatch('updateArrows', group)
        });
        group.add(item);group.add(tr1);
        var nameText = new Konva.Text({
          y:-10, x:-50, width: 100, height: 50,
          text: t.pallete.nodes[t.arrowComp.pallete_index].name, align: "center",
          fontSize: 14, fontFamily: 'Calibri',  fill: 'black'
        });
        var labelText = new Konva.Text({
          y: 5, x:-50, width: 100, height: 40,
          text: "", align: "center",
          fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
          data :{
            name : node.label
          }
        });
      group.add(nameText); group.add(labelText);
        break;
      break;  

      default:
        item = new Konva.Rect({
          x: 0,  y: 0, height: 100, width: 100, resizable : true,
          fill: t.rgbToHex(comps[0], comps[1], comps[2]),
          stroke: t.rgbToHex(comps2[0], comps2[1], comps2[2]), strokeWidth: Number(border_width),  dash :dash
        });
        var tr1 = new Konva.Transformer({ visible: trvisible, node: item, rotateEnabled: false, enabledAnchors: ['bottom-right'],  boundBoxFunc: function(oldBoundBox, newBoundBox) {
        if (Math.abs(newBoundBox.width) < MIN_WIDTH) {
          return oldBoundBox;
        }return newBoundBox;}});
        t.tranformations.push(tr1);
        item.on('transformend', function() {
          t.$store.dispatch('updateTexts', group)
          t.$store.dispatch('updateArrows', group)
        });
        group.add(item);group.add(tr1);
        var nameText = new Konva.Text({
          y:40, x:0, width: 100, height: 50,
          text: t.pallete.nodes[index].name, align: "center",
          fontSize: 14, fontFamily: 'Calibri',  fill: 'black'
        });
        var labelText = new Konva.Text({
          y: 55, x:0, width: 100, height: 35,
          text: "", align: "center",
          fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
          data :{
            name : node.label
          }
        });
        group.add(nameText); group.add(labelText);
        break;
    }
     
    
    t.stage.children[0].children.forEach(chil =>{
      console.log("added group")
      console.log(t.arrowComp.pallete_index)
      if (chil._id == t.arrowComp.source._id){
        t.arrowComp.source.add(group)
      }
      if (chil.children.length>4){
        findSource(chil, group, t);
      }
    })
    //t.stage.add(layer);
    t.stage.draw();
    
    group.on('mouseover', function() {
      if (!t.arrowData.enable)
        document.body.style.cursor = 'pointer';
    });
    group.on('mouseout', function() {
      if (!t.arrowData.enable)
        document.body.style.cursor = 'default';
    });

    group.on('dragmove', function (){
      t.$store.dispatch('updateArrows', group)
    });
    group.on('dragstart', function() {
        //this.moveToTop();
        group.draw();
    });

    group.on('click', function(){
      if (!t.arrowData.enable && !t.arrowComp.enable){
        t.$modal.show('editor', {index: t.arrowComp.pallete_index, group: group});
      }else if(t.arrowData.enable){
        if (t.arrowData.source == undefined){
          t.arrowData.source = group;
        }else{
          if(group.attrs.data.comps.length==0){
            t.arrowData.target = group;
            t.drawArrow();
          }
        }
      }else if(t.arrowComp.enable){ 
         if (t.arrowComp.target == undefined){ 
          t.arrowComp.source = group;
          t.drawComp(t.arrowComp.pallete_index);
          t.saveCompInfo();
        }
      }
    });
    t.arrowComp.target = group;
    group.attrs.data.copy_id = group._id;
  }

  function findSource(element, group, t){
    element.children.forEach(chil =>{
      if (chil._id == t.arrowComp.source._id){
        t.arrowComp.source.add(group)
      }
      if (chil.children.length>4){
        findSource(chil, group, t);
      }
    })
  }
  function saveCompInfo(t) {
    t.stage.children[0].children.forEach(group => {
        if (group._id == t.arrowComp.source._id){
          group.attrs.data.comps.push({
            target_id: t.arrowComp.target._id,
            name : t.pallete.nodes[t.arrowComp.pallete_index].compartment_name,
            eType : t.pallete.nodes[t.arrowComp.pallete_index].eType,
            eSuperTypes : t.pallete.nodes[t.arrowComp.pallete_index].eSuperTypes,
            eName : t.pallete.nodes[t.arrowComp.pallete_index].name,
            parent_id: group.parent._id
            })
        }
        if (group.children.length>=4){
          FindToSave(t,group)
        }
    });
    setTimeout(function() {
    t.resertCompartment();
    }, 500);
  }
  function FindToSave(t, element){
    var i =0;
    element.children.forEach(group => {
      if (group._id == t.arrowComp.source._id){
        console.log(t.pallete.nodes[t.arrowComp.pallete_index])
          group.attrs.data.comps.push({
            target_id: t.arrowComp.target._id,
            name : t.pallete.nodes[t.arrowComp.pallete_index].compartment_name,
            eType : t.pallete.nodes[t.arrowComp.pallete_index].eType,
            eSuperTypes : t.pallete.nodes[t.arrowComp.pallete_index].eSuperTypes, 
            eName : t.pallete.nodes[t.arrowComp.pallete_index].name,
            parent_id: group.parent._id
            })
      }
        if (i>=4){
          FindToSave(t,group)
        }
      i++
  });
  }

  function resertCompartment (t){
    t.arrowComp.enable=false;
    t.arrowComp.source=undefined;
    t.arrowComp.target=undefined;
    t.arrowComp.targeteType = undefined;
    t.arrowComp.index=0;t.arrowComp.pallete_index=0;
    document.body.style.cursor = 'default';
  }

export {
    drawComp,
    saveCompInfo,
    resertCompartment
}