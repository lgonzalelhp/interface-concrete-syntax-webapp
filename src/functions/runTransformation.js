function runTransformation (t, ev, xmi, user){
    
    const axios = require('axios')
   // const fs = require('browserify-fs');
    // const file = ev.target.files[0];
    // const reader = new FileReader();
    // reader.onload = e => {

        var collection = "metamodels";
        var doc = t.selection.name;
        console.log(user)
        var ee = t.selection.name.split("/");
        console.log(t.selection.name)
        if (ee.length >1){
          var c = t.selection.name.split("/");
          doc = c[1];
          collection = c[0]
        }
         console.log(doc)     
        
        var formData = new FormData();
        formData.append('xmi', xmi);
        formData.append('metamodelname', t.selection.ecore_name);

        //formData.append('mtl', e.target.result);
        
        var metamodelRef = t.db.collection(collection).doc(doc);
        var query = metamodelRef.get()
        .then(doc => { 
            t.getUrl(t.selection.name, doc.data().egl.egx).then(data =>{
            formData.append('egx', data)
          });
          
          doc.data().egl.egls.forEach(egl =>{
            t.getUrl(t.selection.name, egl).then(data =>{
              formData.append(egl, data)
            }); 
          })
          t.getUrl(t.selection.name, doc.data().ecore).then(data =>{
            formData.append('ecore', data)
          });
        }).catch(err => {
          console.log('Error getting documents', err);
        });

        setTimeout(function(){ 
            // axios.post( 'https://fathomless-brushlands-59008.herokuapp.com/M2TTransformation',
            //axios.post( 'http://localhost:8080/RemoteEpsilonStandalone-war/M2TTransformation',
            
            //axios.post( 'http://localhost:8080/RemoteEpsilonStandalone-war/RemoteEpsilonTransformation',
            axios.post( 'https://fathomless-brushlands-59008.herokuapp.com/RemoteEpsilonTransformation',
            formData,
            {responseType: 'arraybuffer',
            headers: {
                'Content-Type': 'multipart/form-data'
            }, 
            }
            ).then(function(data){
            const url = window.URL.createObjectURL(new Blob([data.data], {type: "octet/stream"}));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'gen.zip');
            document.body.appendChild(link);
            link.click();
            }).catch(function(error){
            console.log(error)
            });
            
            }, 1500);
            setTimeout(function(){ t.standBy = false;
            }, 2000);
    // };
    // reader.readAsText(file);

}


export {
    runTransformation
}