const XMLExtract = require('xml-extract');
var parseString = require('xml2js').parseString;

var result  = {};
result.nodes = [];
result.links = [];
result.ecore_name = "";
function extracteClassifiers (xmldata){
    XMLExtract(xmldata, 'eClassifiers', true, (error, elements) => {
        elements = elements.replaceAll('xsi:type', 'xsitype')
            parseString(elements, function (err, results) {
            inspecteClassifiers(results)
        });
        XMLExtract(xmldata, 'ecore:EPackage', true, (error, elements) => {
            elements = elements.replaceAll('ecore:EPackage', 'ecoreEPackage')
                parseString(elements, function (err, results) {
                inspectEcore_name(results)
            });
        });
    });
    console.log(result)
    return result;
}
function inspectEcore_name (EPackage) {
    result.ecore_name=EPackage.ecoreEPackage.$.name;
}
function inspecteClassifiers (eClassifiers) {
    var newClassifier = {};
    //if (eClassifiers.eClassifiers.abstract != "true"){
        newClassifier.name = eClassifiers.eClassifiers.$.name;
        newClassifier.abstract = eClassifiers.eClassifiers.$.abstract;
        newClassifier.eSuperTypes = eClassifiers.eClassifiers.$.eSuperTypes;
        newClassifier.attrs = [];
        newClassifier.refs = [];
        if (eClassifiers.eClassifiers.eAnnotations != undefined){
            insepcteAnnotations(eClassifiers, newClassifier);
        }
        if (eClassifiers.eClassifiers.eStructuralFeatures != undefined){
            eClassifiers.eClassifiers.eStructuralFeatures.forEach(eStructuralFeature =>{
                inspecteStructuralFeatures(eStructuralFeature, newClassifier);
            })
        }
    //}
    result.nodes.push(newClassifier);
}


function inspecteStructuralFeatures(eStructuralFeature, newClassifier){
    if(eStructuralFeature.$.xsitype == "ecore:EAttribute"){
        newClassifier.attrs.push({
            'name' : eStructuralFeature.$.name,
            'eType' : eStructuralFeature.$.eType
        })
    }
    
    if(eStructuralFeature.$.xsitype == "ecore:EReference"){
        if (eStructuralFeature.eAnnotations != undefined){
            eStructuralFeature.eAnnotations.forEach(eAnnotation => {
                if (eAnnotation.$.source == "gmf.link") {
                    var reference = {
                        'name' : eStructuralFeature.$.name,
                        'lowerBound' : eStructuralFeature.$.lowerBound,
                        'upperBound' : eStructuralFeature.$.upperBound,
                        'eType' : eStructuralFeature.$.eType,
                        'source' :  newClassifier.name,
                        'containment' : eStructuralFeature.$.containment,
                        'gmf_type': "link"
                    } 
                    if (eAnnotation.details != undefined){
                        eAnnotation.details.forEach(detail =>{
                            if(detail.$.key == "color"){
                                reference.color = detail.$.value;
                            }
                        });
                    }
                    newClassifier.refs.push(reference)
                }
                if (eAnnotation.$.source == "gmf.compartment") {
                    var reference = {
                        'name' : eStructuralFeature.$.name,
                        'lowerBound' : eStructuralFeature.$.lowerBound,
                        'upperBound' : eStructuralFeature.$.upperBound,
                        'eType' : eStructuralFeature.$.eType,
                        'containment' : eStructuralFeature.$.containment,
                        'gmf_type': "compartment"
                    }
                    if (eAnnotation.details != undefined){
                        eAnnotation.details.forEach(detail =>{
                            if(detail.$.key == "color"){
                                reference.color = detail.$.value;
                            }
                        });
                    }
                    newClassifier.refs.push(reference)
                }
            });
        }
    }
        
}

function insepcteAnnotations(eClassifiers, newClassifier){
    eClassifiers.eClassifiers.eAnnotations.forEach(eAnnotation => {
        if (eAnnotation.$.source == "gmf.node") {
            extractNodeInfo(eAnnotation, newClassifier);
        }
        if (eAnnotation.$.source == "gmf.diagram") {
            extractDiagramInfo(eClassifiers.eClassifiers, eAnnotation, newClassifier);
        }
        if (eAnnotation.$.source == "gmf.link") {
            extractLinkInfo(eAnnotation, newClassifier);
        }
    });
}

function extractNodeInfo(eAnnotation,newClassifier) {
    
   if (eAnnotation.details != undefined){
        eAnnotation.details.forEach(detail =>{
        if(detail.$.key == "color"){
            newClassifier.color = detail.$.value;
        }
        if(detail.$.key == "label"){
            newClassifier.label = detail.$.value;
        }
        if(detail.$.key == "figure"){
            newClassifier.figure = detail.$.value;
        }
        if(detail.$.key == "svg.uri"){
            newClassifier.svg_uri = detail.$.value;
        }
        if(detail.$.key == "border.color"){
            newClassifier.border_color = detail.$.value;
        }
        if(detail.$.key == "border.style"){
            newClassifier.border_style = detail.$.value;
        }
        if(detail.$.key == "border.width"){
            newClassifier.border_width = detail.$.value;
        }

        })
    }
    newClassifier.gmf_type = "node";
    //result.nodes.push(newClassifier);
 }
 function extractLinkInfo(eAnnotation,newClassifier) {
    if (eAnnotation.details != undefined){
         eAnnotation.details.forEach(detail =>{
            if(detail.$.key == "color"){
                newClassifier.color = detail.$.value;
            }
            if(detail.$.key == "source"){
                newClassifier.source = detail.$.value;
            }
            if(detail.$.key == "target"){
                newClassifier.target = detail.$.value;
            }
            if(detail.$.key == "widht"){
                newClassifier.width = detail.$.value;
            }
            if(detail.$.key == "style"){
                newClassifier.style = detail.$.value;
            }
            if(detail.$.key == "label"){
                newClassifier.label = detail.$.value;
            }
         })
     }
     newClassifier.gmf_type = "link";
     //result.links.push(newClassifier);
  }

  function extractDiagramInfo(eClassifier, eAnnotation,newClassifier) {
    if (eAnnotation.details != undefined){
        eAnnotation.details.forEach(detail =>{
    
        })
    }
    newClassifier.gmf_type = "diagram";
    eClassifier.eStructuralFeatures.forEach(eStructuralFeature =>{
        if (eStructuralFeature.$.containment == "true"){
            newClassifier.refs.push({
                eType : eStructuralFeature.$.eType,
                name : eStructuralFeature.$.name
            })
        }
    })

    //result.diagram = newClassifier;
  }
 

/*Replace all ocurrences String function */

 String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};


export {
    extracteClassifiers
}