function validate (xmi, t, axios, user){
      var collection = "metamodels";
      var doc = t.selection.name;
      console.log(user)
      var e = t.selection.name.split("/");
      console.log(t.selection.name)
      if (e.length >1){
        var c = t.selection.name.split("/");
        doc = c[1];
        collection = c[0]
      }
       console.log(doc) 
      var formData = new FormData();
      formData.append('xmi', xmi);
      console.log(t.selection.name)
      var metamodelRef = t.db.collection(collection).doc(doc);
      var query = metamodelRef.get()
      .then(doc => {
        t.getUrl(t.selection.name, doc.data().ecore).then(data =>{
          formData.append('ecore', data)
        });
        t.getUrl(t.selection.name, doc.data().evl).then(data =>{
          formData.append('evl', data)
        });
      }).catch(err => {
        console.log('Error getting documents', err);
      });
      setTimeout(function(){ 
        axios.post( 'https://fathomless-brushlands-59008.herokuapp.com/RemoteEpsilon',
        formData,
        {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
      }
    ).then(function(data){
      if (data.data == "All constraints have been satisfied\n"){
        const url = window.URL.createObjectURL(new Blob([xmi]))
        const link = document.createElement('a')
        link.href = url
        link.setAttribute('download', t.selection.ecore_name+'.xmi') //or any other extension
        document.body.appendChild(link)
        link.click()
      }else{
        t.$modal.show('dialog', {
          title: data.data,
          buttons: [
            {
              title: 'OK',
              handler: () => {
                t.$modal.hide('dialog')
              }
            }
          ]
        })
      }
  //console.log('SUCCESS!!');
})
.catch(function(error){
  console.log('FAILURE!! '+error);
});

}, 1500);
setTimeout(function(){ t.standBy = false;
}, 2000);
}

export {
    validate
}