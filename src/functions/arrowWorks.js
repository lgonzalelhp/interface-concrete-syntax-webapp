function drawArrow (t){
    var group = new Konva.Group({
      x: 0, y: 0, height: 100, width: 100, visible: true, data : {grouptype : "arrow"}
    });
    var data = null;
    var labelText = null;
    var comps = t.arrowData.color.split(",");

    if (t.arrowData.color == undefined)
      comps = t.arrowData.color.split("0,0,0");
    var widht = 2;
    if (t.arrowData.widht != undefined){
      widht = t.arrowData.widht
    }

    
    var CS = 100;
    var LS = 50;
    var CT = 0;
    var LT = 50;
    if (t.arrowData.source.children[0].attrs.radius != undefined){
    CS = 50;
    LS = 0;
    }
    if (t.arrowData.target.children[0].attrs.radius != undefined){
      CT = -50;
      LT = 0;
    } 
    
    if (t.arrowData.source.children[0].attrs.scaleX != undefined){
      CS=CS*t.arrowData.source.children[0].attrs.scaleX;
      LS=LS*t.arrowData.source.children[0].attrs.scaleX;
      if (t.arrowData.source.children[0].attrs.radius != undefined){
        LS = LS + (t.arrowData.source.children[0].attrs.scaleX*50)-50;
        CS = CS + (t.arrowData.source.children[0].attrs.scaleX*50)-50;
      }
    }
    
    if (t.arrowData.target.children[0].attrs.scaleX != undefined){
      CT=CT*t.arrowData.target.children[0].attrs.scaleX;
      LT=LT*t.arrowData.target.children[0].attrs.scaleX;
      if (t.arrowData.target.children[0].attrs.radius != undefined){
        LT = LT + (t.arrowData.target.children[0].attrs.scaleX*50)-50;
        CT = CT + (t.arrowData.target.children[0].attrs.scaleX*50)-50;
      }
    }
    var arrow = new Konva.Arrow({
    points: [t.arrowData.source.absolutePosition().x+CS, t.arrowData.source.absolutePosition().y+LS, t.arrowData.target.absolutePosition().x+CT, t.arrowData.target.absolutePosition().y+LT],
    pointerLength: 20, pointerWidth: 20,
    fill:  t.rgbToHex(comps[0], comps[1], comps[2]),
    stroke: t.rgbToHex(comps[0], comps[1], comps[2]),
    strokeWidth: Number(widht), /*dash: dash,*/
    data : {
      source_id : t.arrowData.source._id,
      source_x : t.arrowData.source.absolutePosition().x,
      source_y : t.arrowData.source.absolutePosition().y,
      source_radius : t.arrowData.source.attrs.radius,
      source_scaleX : t.arrowData.source.attrs.scaleX,
      source_scaleY : t.arrowData.source.attrs.scaleY,
      target_id : t.arrowData.target._id, 
      target_x : t.arrowData.target.absolutePosition().x,
      target_y : t.arrowData.target.absolutePosition().y,
      target_scaleX : t.arrowData.target.attrs.scaleX,
      target_scaleY : t.arrowData.target.attrs.scaleY,
      target_radius : t.arrowData.target.attrs.radius,
      sourcePosition: "right",
      targetPosition: "left",
      type : "link"
    }
    });
    
    if (t.arrowData.style == "line"){
      console.log("ITS A DASH")
      arrow.attrs.dash= [10,5]
    }
    if (t.link_element.style == "dot"){
      arrow.attrs.dash= [1,2]
      //dash=[1,2]
    }
    var value = "";
    t.containments.forEach(containment=>{
      if (containment.eType == "#//"+t.arrowData.target.attrs.data.eType || containment.eType == t.arrowData.target.attrs.data.eSuperTypes){
        value = containment.name; 
      }
    })
    t.pallete.compartments.forEach(compartment=>{
      if (compartment.eType == "#//"+t.arrowData.target.attrs.data.eType || compartment.eType == t.arrowData.target.attrs.data.eSuperTypes){
        value = compartment.name; 
      }
    })
    t.stage.children[0].children.forEach(group => {
        if (group._id == t.arrowData.source._id){
          if (!t.arrowData.isClass){
            group.attrs.data.refs.push({
                name : t.link_element.name,
                value : "//@"+value,
                element_id : group._id,
                target_id : t.arrowData.target._id,
                eType: t.arrowData.target.attrs.data.eType,
                eSuperTypes: t.arrowData.target.attrs.data.eSuperTypes
              })
          }
          else{
            data = {
              name : t.link_element.name,
              value : "//@"+value,
              element_id : group._id,
              source_id : t.arrowData.source._id,
              target_id : t.arrowData.target._id,
              eType: t.link_element.name,
              eSuperTypes: t.link_element.eSuperTypes,
              attrs : [],
              refs : [],
              label : t.link_element.label
            }
            data.refs.push({
                name : t.link_element.name,
                value : "//@"+value,
                element_id : group._id,
                target_id : t.arrowData.target._id,
                source: {
                  name: t.link_element.source,
                  source_id : t.arrowData.source._id,
                  eType: t.arrowData.source.attrs.data.eType,
                  eSuperTypes: t.arrowData.source.attrs.data.eSuperTypes,
                },
                target: {
                  name: t.link_element.target,
                  target_id : t.arrowData.target._id,
                  eType: t.arrowData.target.attrs.data.eType,
                  eSuperTypes: t.arrowData.target.attrs.data.eSuperTypes,
                }
              })
             if(data.attrs.length == 0){
              var node = t.link_element;
              if (node.eSuperTypes != undefined){
                t.$store.state.pallete.nodes.forEach(element => {
                  if ("#//"+element.name == node.eSuperTypes){
                    element.attrs.forEach(att=>{
                      data.attrs.push({
                        name : att.name,
                        value : ''
                      });
                    })
                  }
                });
              }else{
                node.attrs.forEach(att =>{
                  data.attrs.push({
                        name : att.name,
                        value : ''
                      });
                })
              }
             }
              t.classes_links.push(data)
              labelText = new Konva.Text({
                x:t.arrowData.source.attrs.x+CS+2, y: t.arrowData.source.attrs.y+LS+2, width:48, height:48, 
              text: "", align: "center",
              fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
              data :{
                name : t.link_element.label
              }
              });
          }
       }
       if (group.children.length>=4){
        FindToAdd(t,group, value)
        }
    });

    
    t.$store.dispatch('addArrow', arrow);
    
    group.add(arrow)
    arrow.on('click', function(){
      t.showArrowDialog(arrow, data, group)
    });
    group.on('mouseover', function() {
      if (!t.arrowData.enable)
        document.body.style.cursor = 'pointer';
    });
    group.on('mouseout', function() {
      if (!t.arrowData.enable)
        document.body.style.cursor = 'default';
    });
    if (labelText!=null)
      group.add(labelText)
    t.stage.children[0].add(group);
    t.stage.draw();
    setTimeout(function() {
      t.resert();
      }, 500);
    
}
function FindToAdd(t, element, value){
var i =0;
element.children.forEach(group => {
  if (group._id == t.arrowData.source._id){
    if (!t.arrowData.isClass){
      group.attrs.data.refs.push({
          name : t.link_element.name,
          value : "//@"+value,
          element_id : group._id,
          target_id : t.arrowData.target._id,
          eType: t.arrowData.target.attrs.data.eType,
          eSuperTypes: t.arrowData.target.attrs.data.eSuperTypes
        })
    }
    else{
      data = {
        name : t.link_element.name,
        value : "//@"+value,
        element_id : group._id,
        source_id : t.arrowData.source._id,
        target_id : t.arrowData.target._id,
        eType: t.link_element.name,
        eSuperTypes: t.link_element.eSuperTypes,
        attrs : [],
        refs : [],
        label : t.link_element.label
      }
      data.refs.push({
          name : t.link_element.name,
          value : "//@"+value,
          element_id : group._id,
          target_id : t.arrowData.target._id,
          source: {
            name: t.link_element.source,
            source_id : t.arrowData.source._id,
            eType: t.arrowData.source.attrs.data.eType,
            eSuperTypes: t.arrowData.source.attrs.data.eSuperTypes,
          },
          target: {
            name: t.link_element.target,
            target_id : t.arrowData.target._id,
            eType: t.arrowData.target.attrs.data.eType,
            eSuperTypes: t.arrowData.target.attrs.data.eSuperTypes,
          }
        })
       if(data.attrs.length == 0){
        var node = t.link_element;
        if (node.eSuperTypes != undefined){
          t.$store.state.pallete.nodes.forEach(element => {
            if ("#//"+element.name == node.eSuperTypes){
              element.attrs.forEach(att=>{
                data.attrs.push({
                  name : att.name,
                  value : ''
                });
              })
            }
          });
        }else{
          node.attrs.forEach(att =>{
            data.attrs.push({
                  name : att.name,
                  value : ''
                });
          })
        }
       }
        t.classes_links.push(data)
        labelText = new Konva.Text({
          x:t.arrowData.source.attrs.x+CS+2, y: t.arrowData.source.attrs.y+LS+2, width:48, height:48, 
        text: "", align: "center",
        fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
        data :{
          name : t.link_element.label
        }
        });
    }
 }
 if (i>=4){
  FindToAdd(t,group, value)
  }
  i++;
});

}
function showArrowDialog (t, arrow, data, group){
  if (t.arrowData.isClass || group.children.length == 2)
    t.$modal.show('dialog', {
     title: 'Edit link',
     buttons: [
       {
         title: 'Move arrow source',
         handler: () => {
          t.$store.dispatch('rotateSource', arrow);      
         }
       },
       {
         title: 'Edit attributes',
         handler: () => {
         t.$modal.show('editor1', {index: t.arrowData.index, realgroup: group, group: data});  
         t.$modal.hide('dialog')
         }
       },
       {
         title: 'DELETE',
         default: true,
         handler: () => {
           t.$store.dispatch('deleteArrow', arrow);
           t.$store.dispatch('deleteClink', data);
           t.$modal.hide('dialog')
         }
       },
       {
         title: 'Move arrow target',
         handler: () => {
           t.$store.dispatch('rotateTarget', arrow);
         }
       }
     ]
   })
   else
     t.$modal.show('dialog', {
     title: 'Edit link',
     buttons: [
       {
         title: 'Move arrow source',
         handler: () => {
          t.$store.dispatch('rotateSource', arrow);      
         }
       },
       {
         title: 'DELETE',
         default: true,
         handler: () => {
           t.$store.dispatch('deleteArrow2', arrow);
           t.$modal.hide('dialog')
         }
       },
       {
         title: 'Move arrow target',
         handler: () => {
           t.$store.dispatch('rotateTarget', arrow);
         }
       }
     ]
   })
}

function resert (t){
    t.arrowData.enable=false;
    t.arrowData.source=undefined;
    t.arrowData.target=undefined;
    t.arrowData.color="0,0,0";
    t.arrowData.style = "line";
    t.arrowData.width = 2;
    t.arrowData.targeteType = undefined;
    t.arrowData.index=0;
    document.body.style.cursor = 'default';
}

export {
    drawArrow,
    showArrowDialog,
    resert
}