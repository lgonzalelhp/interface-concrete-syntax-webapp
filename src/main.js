import Vue from 'vue'
import App from './App.vue'
import VueKonva from 'vue-konva'
import VModal from 'vue-js-modal'
import {store} from './store/store'


import VueResource from 'vue-resource'
import router from './routes';

import { FormFilePlugin } from 'bootstrap-vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'bootstrap'; import 'bootstrap/dist/css/bootstrap.min.css';
Vue.use(Vuetify)
Vue.use(FormFilePlugin)

Vue.use(VueKonva)
Vue.use(VModal, {dialog: true})


Vue.use(VueResource);

let app = '';
const firebaseInit = require ('./functions/firebaseInit')

var auth = firebaseInit.initializeApp.auth();


auth.onAuthStateChanged(() => {
  if (!app) {
    /* eslint-disable no-new */
    app = new Vue({
      router,
      store : store,
      render: h => h(App)
    }).$mount('#app');
  }
});