import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({

    state: {
        selection :{
            name : "",
            ecore : "",
            evl : "",
            ecore_name: "",
            diagram_name: "",
            svg : []
        },
        loaded : "0",
        currentUser: "",
        pallete:{},
        stage :{},
        containments : [],
        numelements : 0,
        arrows : [],
        classes_links : [],
    },
    getters:{
        
    },
    mutations:{

        loadPallete: (state, payload) => {
            state.pallete = payload;
            state.selection.ecore_name = payload.ecore_name;
            state.pallete.compartments = [];
            payload.nodes.forEach(node=>{
                //console.log(node)
                node.isComp = false;
                if(node.refs.length >0){
                    node.refs.forEach(ref=>{
                        if(ref.containment == undefined){
                            state.pallete.links.push(ref)
                        }
                        if(ref.containment == "true"){
                            state.pallete.compartments.push(ref);
                        }
                    })
                }
                if (node.gmf_type == "diagram"){
                    state.containments = node.refs;
                    state.selection.diagram_name = node.name;
                }
            })
            payload.nodes.forEach(node=>{
                payload.compartments.forEach(comp=>{
                    if("#//"+node.name == comp.eType || node.eSuperTypes == comp.eType){
                        node.isComp = true;
                        node.compartment_name =comp.name;
                    }
                   
                });
            });
            
        },
        loadStage : (state, payload) =>{
            state.stage = payload;
        },
       
        deleteFromStage : (state, payload) =>{
            state.arrows.forEach(arrow =>{
                if (arrow.attrs.data.source_id  == payload.group._id || arrow.attrs.data.target_id  == payload.group._id){    
                    if (arrow.parent!=null)
                    arrow.parent.remove();
                    var i = 0;
                    var d = 0;
                    state.classes_links.forEach(clink =>{
                        if (clink.source_id == arrow.attrs.data.source_id || clink.target_id == arrow.attrs.data.target_id){
                            d = i;
                        }
                        i++;
                    })
                    state.classes_links.splice(d,1);
                }
            })
            
            payload.source.children.forEach(element =>{ 
                if(element == payload.group){
                    element.remove();
                    state.stage.draw();
                }
            })
            var isComp = false;
            state.pallete.compartments.forEach(comp=>{
                if (comp.eType == payload.group.attrs.data.eSuperTypes || comp.eType == "#//"+payload.group.attrs.data.eType){
                    isComp=true;
                }
            })
            if (isComp){
                payload.source.children.forEach(chil=>{
                    if (chil.attrs.data != undefined){
                        var i =0; var d= 0; var find =false;
                        if (chil.attrs.data.comps != undefined){
                            chil.attrs.data.comps.forEach(comp =>{
                                if(comp.target_id == payload.group._id){
                                    d=i;
                                    find = true;
                                }
                                i++;
                            })
                            if (find){
                                chil.attrs.data.comps.splice(d,1); 
                                find = false;
                            }
                        }   
                    }
                    
                })
            }
        },
        addDataToLayer : (state, payload) =>{
            state.stage.children[0].children.forEach(element =>{
                if(element.attrs.id == payload.id){
                    element.attrs.data = payload.result;
                }
                
            })  
        },
        incrementNumElements: (state) =>{
            state.numelements++;
        },
        decrementNumElements: (state) =>{
            state.numelements--;
        },
        addArrow : (state, payload) =>{
            state.arrows.push(payload);
        },
        updateArrows : (state, payload) =>{
            var C = 100;
            var L = 50;
            var K=0;
            var brk = false;
            
            state.arrows.forEach(arrow =>{
                if (arrow.attrs.data.source_id == payload._id){ 
                    if (payload.children[0].attrs.scaleX != undefined && !brk){
                        //K=K*payload.children[0].attrs.scaleX;
                        C=C*payload.children[0].attrs.scaleX;
                        L=L*payload.children[0].attrs.scaleX;
                        brk = true;
                    }
                    if (payload.children[0].attrs.radius != undefined){
                        K = 50;
                        C=C+K-50;
                        L=L+K-50;
                    }
                    if (arrow.attrs.data.sourcePosition == "right"){
                        arrow.attrs.points = [payload.absolutePosition().x+C-K,payload.absolutePosition().y+L-K, arrow.attrs.points[2],arrow.attrs.points[3]]
                        if (arrow.parent != null && arrow.parent.children[1] != undefined){
                            var x = (arrow.parent.children[0].attrs.points[0]+arrow.parent.children[0].attrs.points[2]-24)/2
                            var y = (arrow.parent.children[0].attrs.points[1]+arrow.parent.children[0].attrs.points[3]-24)/2
                            var labelText = new Konva.Text({
                                y: y, width: L, height: C, x: x,
                                text: arrow.parent.children[1].attrs.text, align: "center",
                                fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                            });
                            arrow.parent.children.pop();
                            arrow.parent.add(labelText);
                        }
                    }
                    if (arrow.attrs.data.sourcePosition == "bottom"){
                        arrow.attrs.points = [payload.absolutePosition().x+L-K,payload.absolutePosition().y+C-K, arrow.attrs.points[2],arrow.attrs.points[3]]
                        if (arrow.parent != null && arrow.parent.children[1] != undefined){
                            var x = (arrow.parent.children[0].attrs.points[0]+arrow.parent.children[0].attrs.points[2]-24)/2
                            var y = (arrow.parent.children[0].attrs.points[1]+arrow.parent.children[0].attrs.points[3]-24)/2
                            var labelText = new Konva.Text({
                                y: y, width: L, height: C, x: x,
                                text: arrow.parent.children[1].attrs.text, align: "center",
                                fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                            });
                            arrow.parent.children.pop();
                            arrow.parent.add(labelText);
                        }
                    }
                    if (arrow.attrs.data.sourcePosition == "left"){
                        if (payload.children[0].attrs.radius != undefined)
                            arrow.attrs.points = [arrow.attrs.data.source_x-50,payload.absolutePosition().y+L-K, arrow.attrs.points[2],arrow.attrs.points[3]]
                        else
                            arrow.attrs.points = [arrow.attrs.data.source_x,payload.absolutePosition().y+L-K, arrow.attrs.points[2],arrow.attrs.points[3]]
                        if (arrow.parent.children[1] != undefined){
                            var x = (arrow.parent.children[0].attrs.points[0]+arrow.parent.children[0].attrs.points[2]-24)/2
                            var y = (arrow.parent.children[0].attrs.points[1]+arrow.parent.children[0].attrs.points[3]-24)/2
                            var labelText = new Konva.Text({
                                y: y, width: L, height: C, x: x,
                                text: arrow.parent.children[1].attrs.text, align: "center",
                                fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                            });
                            arrow.parent.children.pop();
                            arrow.parent.add(labelText);
                        }
                    }
                    if (arrow.attrs.data.sourcePosition == "top"){
                        if (payload.children[0].attrs.radius != undefined)
                            arrow.attrs.points = [payload.absolutePosition().x+L-K,arrow.attrs.data.source_y-50, arrow.attrs.points[2],arrow.attrs.points[3]]
                        else
                            arrow.attrs.points = [payload.absolutePosition().x+L-K,arrow.attrs.data.source_y, arrow.attrs.points[2],arrow.attrs.points[3]]
                        if (arrow.parent.children[1] != undefined){
                            var x = (arrow.parent.children[0].attrs.points[0]+arrow.parent.children[0].attrs.points[2]-24)/2
                            var y = (arrow.parent.children[0].attrs.points[1]+arrow.parent.children[0].attrs.points[3]-24)/2
                            var labelText = new Konva.Text({
                                y: y, width: L, height: C, x: x,
                                text: arrow.parent.children[1].attrs.text, align: "center",
                                fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                                });
                            arrow.parent.children.pop();
                            arrow.parent.add(labelText);
                        }
                    }
                    arrow.attrs.data.source_x = payload.absolutePosition().x;
                    arrow.attrs.data.source_y = payload.absolutePosition().y;
                    arrow.attrs.data.source_scaleX = payload.children[0].attrs.scaleX;
                    arrow.attrs.data.source_radius = payload.children[0].attrs.radius;
                }
                if(arrow.attrs.data.target_id == payload._id){
                    K=0
                    if (payload.children[0].attrs.scaleX != undefined && !brk){
                        //K=K*payload.children[0].attrs.scaleX;console.log("K: "+K)
                        C=C*payload.children[0].attrs.scaleX;
                        L=L*payload.children[0].attrs.scaleX;
                        brk = true;
                    }
                    if (payload.children[0].attrs.radius != undefined){
                        K = 50;
                        C=C+K-50;
                        L=L+K-50;
                    }
                    if (arrow.parent != null)
                    if (arrow.attrs.data.targetPosition == "right"){
                        arrow.attrs.points = [arrow.attrs.points[0],arrow.attrs.points[1],payload.absolutePosition().x+C-K,payload.absolutePosition().y+L-K]
                        if (arrow.parent.children[1] != undefined){
                            var x = (arrow.parent.children[0].attrs.points[0]+arrow.parent.children[0].attrs.points[2]-24)/2
                            var y = (arrow.parent.children[0].attrs.points[1]+arrow.parent.children[0].attrs.points[3]-24)/2
                            var labelText = new Konva.Text({
                                y: y, width: L, height: C, x: x,
                                text: arrow.parent.children[1].attrs.text, align: "center",
                                fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                                });
                            arrow.parent.children.pop();
                            arrow.parent.add(labelText);
                            }
                        }
                    if (arrow.attrs.data.targetPosition == "bottom"){
                        arrow.attrs.points = [arrow.attrs.points[0],arrow.attrs.points[1],payload.absolutePosition().x+L-K,payload.absolutePosition().y+C-K]
                        if (arrow.parent != null)
                        if (arrow.parent.children[1] != undefined){
                            var x = (arrow.parent.children[0].attrs.points[0]+arrow.parent.children[0].attrs.points[2]-24)/2
                            var y = (arrow.parent.children[0].attrs.points[1]+arrow.parent.children[0].attrs.points[3]-24)/2
                            var labelText = new Konva.Text({
                                y: y, width: L, height: C, x: x,
                                text: arrow.parent.children[1].attrs.text, align: "center",
                                fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                                });
                            arrow.parent.children.pop();
                            arrow.parent.add(labelText);
                            }
                        }
                    if (arrow.attrs.data.targetPosition == "left"){
                        if (payload.children[0].attrs.radius != undefined)
                            arrow.attrs.points = [arrow.attrs.points[0],arrow.attrs.points[1],arrow.attrs.data.target_x-50,payload.absolutePosition().y+L-K]
                        else
                            arrow.attrs.points = [arrow.attrs.points[0],arrow.attrs.points[1],payload.absolutePosition().x,payload.absolutePosition().y+L]
                        if (arrow.parent != null)
                        if (arrow.parent.children[1] != undefined){
                            var x = (arrow.parent.children[0].attrs.points[0]+arrow.parent.children[0].attrs.points[2]-24)/2
                            var y = (arrow.parent.children[0].attrs.points[1]+arrow.parent.children[0].attrs.points[3]-24)/2
                            var labelText = new Konva.Text({
                                y: y, width: L, height: C, x: x,
                                text: arrow.parent.children[1].attrs.text, align: "center",
                                fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                                });
                            arrow.parent.children.pop();
                            arrow.parent.add(labelText);
                            }
                        }
                    if (arrow.attrs.data.targetPosition == "top"){
                        if (payload.children[0].attrs.radius != undefined)
                            arrow.attrs.points = [arrow.attrs.points[0],arrow.attrs.points[1],payload.absolutePosition().x+L-K,arrow.attrs.data.target_y-50]
                        else
                            arrow.attrs.points = [arrow.attrs.points[0],arrow.attrs.points[1],payload.absolutePosition().x+L-K,arrow.attrs.data.target_y]
                        if (arrow.parent != null)
                        if (arrow.parent.children[1] != undefined){
                            var x = (arrow.parent.children[0].attrs.points[0]+arrow.parent.children[0].attrs.points[2]-24)/2
                            var y = (arrow.parent.children[0].attrs.points[1]+arrow.parent.children[0].attrs.points[3]-24)/2
                            var labelText = new Konva.Text({
                                y: y, width: L, height: C, x: x,
                                text: arrow.parent.children[1].attrs.text, align: "center",
                                fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                                });
                            arrow.parent.children.pop();
                            arrow.parent.add(labelText);
                            }
                        }
                        arrow.attrs.data.target_x = payload.absolutePosition().x;
                        arrow.attrs.data.target_y = payload.absolutePosition().y;
                        arrow.attrs.data.target_scaleX = payload.children[0].attrs.scaleX;
                        arrow.attrs.data.target_radius = payload.children[0].attrs.radius;
                }
               
                state.stage.draw();
            })
            brk=false;
        },
        deleteArrow : (state, payload) =>{
            if (payload.attrs.data.type == "link"){
                console.log("DELETE LINK")
                state.stage.children[0].children.forEach(chil => {
                    if (chil._id == payload.attrs.data.source_id){
                        var i = 0, d=0;
                        chil.attrs.data.refs.forEach(ref=>{
                            if (ref.target_id ==  payload.attrs.data.target_id){
                                d=i;
                            }
                            i++;
                        });
                        chil.attrs.data.refs.splice(d,1);
                    }
                })
            }
            
            var i = 0;
            var d = 0;
            state.arrows.forEach(arrow =>{
                if (arrow == payload){
                    if(arrow.parent.children.length == 2){
                        d=i;
                        arrow.parent.children.pop();arrow.remove();
                    }
                    state.stage.draw();
                }i++;
                state.stage.draw();
            })
            state.arrows.splice(d,1);
            state.arrows.forEach(arrow =>{
                console.log(arrow)
            });
            
        },

        deleteArrow2 : (state, payload) =>{
            if (payload.attrs.data.type == "link"){
                state.stage.children[0].children.forEach(chil => {
                    if (chil._id == payload.attrs.data.source_id){
                        var i = 0, d=0;
                        chil.attrs.data.refs.forEach(ref=>{
                            if (ref.target_id ==  payload.attrs.data.target_id){
                                d=i;
                            }
                            i++;
                        });
                        chil.attrs.data.refs.splice(d,1);
                    }
                })
            }
            state.arrows.forEach(arrow =>{
                if (arrow == payload){
                    arrow.remove();
                    state.stage.draw();
                }
                state.stage.draw();
            })
        },
        deleteClink : (state, payload) =>{
            var i = 0;
            var d = 0;
            state.classes_links.forEach(clink =>{
                console.log(clink)
                console.log(payload)
                if (clink == payload){
                    d = i;
                }
                i++;
            })
            state.classes_links.splice(d,1);
        },
        rotateSource : (state, payload) => {
            state.arrows.forEach(arrow =>{
                if (arrow == payload){
                    var C = 100;
                    var L = 50;
                    var K = -50;
                    // var source = null;
                    // state.stage.children[0].children.forEach(group =>{
                    //     if (group._id == payload.attrs.data.source_id){
                    //         source = group;
                    //         console.log("matchs")
                    //     }
                    // })
                    // console.log(source);
                    //console.log(state.stage.children[0].find(payload.attrs.data.source_id))
                    if (arrow.attrs.data.source_scaleX != undefined){
                        C=C*arrow.attrs.data.source_scaleX;
                        L=L*arrow.attrs.data.source_scaleX;
                    }
                    var ax = arrow.attrs.points[0]; var ay = arrow.attrs.points[1];
                    var sx = arrow.attrs.data.source_x; var sy = arrow.attrs.data.source_y
                    if (arrow.attrs.data.source_radius != undefined){
                        //console.log("RADIUS")
                        sx =sx+K; sy = sy+K
                    }
                    //    console.log("ax "+ax)
                    //    console.log("ay "+ay) 
                    //    console.log("sx "+sx)
                    //    console.log("sy "+sy) 
                    if ((ax-(sx+C) < 1) && (ay - (sy+L)) <1){
                        arrow.attrs.points = [ax-L, ay+L, arrow.attrs.points[2], arrow.attrs.points[3]]
                        arrow.attrs.data.sourcePosition = "bottom"
                        if (arrow.parent.children[1] != undefined){
                            var x = (arrow.parent.children[0].attrs.points[0]+arrow.parent.children[0].attrs.points[2]-24)/2
                            var y = (arrow.parent.children[0].attrs.points[1]+arrow.parent.children[0].attrs.points[3]-24)/2
                            var labelText = new Konva.Text({
                                y: y, width: L, height: C, x: x,
                                text: arrow.parent.children[1].attrs.text, align: "center",
                                fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                                });
                            arrow.parent.children.pop();
                            arrow.parent.add(labelText);
                        }
                    }
                    if ((ax - (sx+L))<1 && (ay - (sy+C))<1){
                        arrow.attrs.points = [ax-L, ay-L, arrow.attrs.points[2], arrow.attrs.points[3]]
                        arrow.attrs.data.sourcePosition = "left"
                        if (arrow.parent.children[1] != undefined){
                            var x = (arrow.parent.children[0].attrs.points[0]+arrow.parent.children[0].attrs.points[2]-24)/2
                            var y = (arrow.parent.children[0].attrs.points[1]+arrow.parent.children[0].attrs.points[3]-24)/2
                            var labelText = new Konva.Text({
                                y: y, width: L, height: C, x: x,
                                text: arrow.parent.children[1].attrs.text, align: "center",
                                fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                                });
                            arrow.parent.children.pop();
                            arrow.parent.add(labelText);
                        }
                    }
                    if ((ax - sx) <1 && (ay - (sy+L))<1){
                        arrow.attrs.points = [ax+L, ay-L, arrow.attrs.points[2], arrow.attrs.points[3]]
                        arrow.attrs.data.sourcePosition = "top"
                        if (arrow.parent.children[1] != undefined){    
                            var x = (arrow.parent.children[0].attrs.points[0]+arrow.parent.children[0].attrs.points[2]-24)/2
                            var y = (arrow.parent.children[0].attrs.points[1]+arrow.parent.children[0].attrs.points[3]-24)/2
                            var labelText = new Konva.Text({
                                y: y, width: L, height: C, x: x,
                                text: arrow.parent.children[1].attrs.text, align: "center",
                                fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                                });
                            arrow.parent.children.pop();
                            arrow.parent.add(labelText);
                        }
                    }
                    if ((ax - (sx+L))<1 && (ay - sy)<1){     
                        console.log("Right")               
                        arrow.attrs.points = [ax+L, ay+L, arrow.attrs.points[2], arrow.attrs.points[3]]
                        arrow.attrs.data.sourcePosition = "right"
                        if (arrow.parent.children[1] != undefined){
                            var x = (arrow.parent.children[0].attrs.points[0]+arrow.parent.children[0].attrs.points[2]-24)/2
                            var y = (arrow.parent.children[0].attrs.points[1]+arrow.parent.children[0].attrs.points[3]-24)/2
                            var labelText = new Konva.Text({
                                y: y, width: L, height: C, x: x,
                                text: arrow.parent.children[1].attrs.text, align: "center",
                                fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                                });
                            arrow.parent.children.pop();
                            arrow.parent.add(labelText);
                        }
                    }
                }
            });
            state.stage.draw();
        },
        rotateTarget : (state, payload) => {
            state.arrows.forEach(arrow =>{
                if (arrow == payload){
                    var C = 100;
                    var L = 50;
                    var K = -50;
                    // var target = null;
                    // var target = state.stage.find('#'+payload.attrs.data.target_id)[0];
                    // state.stage.children[0].children.forEach(group =>{
                    //     if (group._id == payload.attrs.data.target_id){
                    //         target = group;
                    //     }
                    // })
                    if ( arrow.attrs.data.target_scaleX!= undefined){
                        C=C*arrow.attrs.data.target_scaleX;
                        L=L*arrow.attrs.data.target_scaleX;
                    }
                    var ax = arrow.attrs.points[2]; var ay = arrow.attrs.points[3];
                    var sx = arrow.attrs.data.target_x; var sy = arrow.attrs.data.target_y
                    if (arrow.attrs.data.target_radius != undefined){
                        sx =sx+K; sy = sy+K
                    }
                    if ((ax - (sx+C))<1 && (ay - (sy+L))<1){
                        arrow.attrs.points = [arrow.attrs.points[0], arrow.attrs.points[1], ax-L, ay+L]
                        arrow.attrs.data.targetPosition = "bottom"
                        if (arrow.parent.children[1] != undefined){
                            var x = (arrow.parent.children[0].attrs.points[0]+arrow.parent.children[0].attrs.points[2]-24)/2
                            var y = (arrow.parent.children[0].attrs.points[1]+arrow.parent.children[0].attrs.points[3]-24)/2
                            var labelText = new Konva.Text({
                                y: y, width: L, height: C, x: x,
                                text: arrow.parent.children[1].attrs.text, align: "center",
                                fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                                });
                            arrow.parent.children.pop();
                            arrow.parent.add(labelText);
                        }
                    }
                    if ((ax - (sx+L))<1 && (ay - (sy+C))<1){
                        arrow.attrs.points = [arrow.attrs.points[0], arrow.attrs.points[1], ax-L, ay-L]
                        arrow.attrs.data.targetPosition = "left"
                        if (arrow.parent.children[1] != undefined){
                            var x = (arrow.parent.children[0].attrs.points[0]+arrow.parent.children[0].attrs.points[2]-24)/2
                            var y = (arrow.parent.children[0].attrs.points[1]+arrow.parent.children[0].attrs.points[3]-24)/2
                            var labelText = new Konva.Text({
                                y: y, width: L, height: C, x: x,
                                text: arrow.parent.children[1].attrs.text, align: "center",
                                fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                                });
                            arrow.parent.children.pop();
                            arrow.parent.add(labelText);
                        }
                    }
                    if ((ax - sx)<1 && (ay -(sy+L))<1){
                        arrow.attrs.points = [arrow.attrs.points[0], arrow.attrs.points[1], ax+L, ay-L]
                        arrow.attrs.data.targetPosition = "top"
                        if (arrow.parent.children[1] != undefined){
                            var x = (arrow.parent.children[0].attrs.points[0]+arrow.parent.children[0].attrs.points[2]-24)/2
                            var y = (arrow.parent.children[0].attrs.points[1]+arrow.parent.children[0].attrs.points[3]-24)/2
                            var labelText = new Konva.Text({
                                y: y, width: L, height: C, x: x,
                                text: arrow.parent.children[1].attrs.text, align: "center",
                                fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                                });
                            arrow.parent.children.pop();
                            arrow.parent.add(labelText);
                        }
                    }
                    if ((ax - (sx+L))<1 && (ay - sy)<1){
                        arrow.attrs.points = [arrow.attrs.points[0], arrow.attrs.points[1], ax+L, ay+L]
                        arrow.attrs.data.targetPosition = "right"
                        if (arrow.parent.children[1] != undefined){
                            var x = (arrow.parent.children[0].attrs.points[0]+arrow.parent.children[0].attrs.points[2]-24)/2
                            var y = (arrow.parent.children[0].attrs.points[1]+arrow.parent.children[0].attrs.points[3]-24)/2
                            var labelText = new Konva.Text({
                                y: y, width: L, height: C, x: x,
                                text: arrow.parent.children[1].attrs.text, align: "center",
                                fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                                });
                            arrow.parent.children.pop();
                            arrow.parent.add(labelText);
                        }
                    }
                }
            });
            state.stage.draw();
        },
        hideNodes: (state, payload) =>{
            state.stage.children[0].children.forEach(element =>{
                if (element.attrs.data != undefined) {
                    if (element.attrs.data.eType == payload || element.attrs.data.eSuperTypes == payload){
                        element.children.forEach(chil=>{
                            chil.show();
                        })
                    }else{
                        element.children.forEach(chil=>{
                            chil.hide();
                        })
                        
                    } 
                }
            })  
            state.stage.draw();
        },
        showAllNodes: (state) =>{
            state.stage.children[0].children.forEach(element =>{
                element.children.forEach(chil=>{
                    chil.show();
                })
            })  
            state.stage.draw();
        },
        updateTexts : (state, payload) =>{
            console.log(payload)
            if (payload.children.length >= 4){
                payload.children[2].attrs.width = 100 * payload.children[0].attrs.scaleX ;
               //payload.children[2].attrs.height = 100* payload.children[0].attrs.scaleY;
                payload.children[2].attrs.y = (50 * payload.children[0].attrs.scaleY )-7;
                
                payload.children[3].attrs.width = 100 * payload.children[0].attrs.scaleX ;
                //payload.children[3].attrs.height = 100* payload.children[0].attrs.scaleY;
                payload.children[3].attrs.y = (50* payload.children[0].attrs.scaleY) +10;
            } 
            state.stage.draw();
        },
        updateArrowLabel : (state, payload) =>{
            var brk = false;
            var value = "";
            payload.data.attrs.forEach (attr =>{
                if (attr.name == payload.data.label){
                    value = attr.value;
                }
            })
            state.stage.children[0].children.forEach(chil => {
                console.log(payload)
                if (chil == payload.group){
                    if (chil.children.length == 2){
                        var CS = 100;
                        var LS = 50;
                        var source = null;
                        state.stage.children[0].children.forEach(group =>{
                            if (group._id == payload.data.source_id){
                                source = group;
                                console.log("matchs")
                            }
                        })
                        if (source.attrs.scaleX != undefined){
                            state.stage.children[0].children.forEach(group=>{
                                if (group._id == source._id && !brk ){
                                    CS=CS*group.children[0].attrs.scaleX;
                                    LS=LS*group.children[0].attrs.scaleX;
                                    brk = true;
                                }
                                
                            })
                        }
                        var x = (payload.group.children[0].attrs.points[0]+payload.group.children[0].attrs.points[2]-24)/2
                        var y = (payload.group.children[0].attrs.points[1]+payload.group.children[0].attrs.points[3]-24)/2
                        var labelText = new Konva.Text({
                            x:x, y: y, width:48, height:48 ,
                            text: value, align: "center",
                            fontSize: 10, fontFamily: 'Calibri',  fill: 'black',
                            data :{
                              name : payload.label
                            }
                          });
                          chil.children.pop();
                          chil.add(labelText);
                    } 
                }
            })
            state.stage.draw();
        },
        setSelection : (state, payload) =>{
            if (state.selection.name != payload.name || state.selection.ecore != payload.ecore){
                state.pallete.compartments =[];
                state.pallete.links = [];
                state.pallete.nodes = [];
                state.pallete = {};
            }
            state.selection.name = payload.name;
            state.selection.ecore = payload.ecore;
            state.selection.evl = payload.evl;
            state.selection.svg = payload.svg;
            state.selection.egl = payload.egl;
        },

        restartClinks : (state) => {
            state.classes_links = [];
        },
        restartStage : (state) => {
            state.arrows = [];
            state.stage.children[0].remove();
            var layer = new Konva.Layer();
            state.stage.add(layer);
            state.stage.draw();
        },
        setUser : (state, payload) =>{
           state.currentUser = payload;
        },
    
    },
    actions:{
        loadPallete: (context, payload)=>{
            context.commit('loadPallete', payload);
        },
        loadStage : (context, payload) =>{
            context.commit('loadStage', payload);
        },
        deleteFromStage : (context, payload) =>{
            context.commit('deleteFromStage', payload);
        },
        addDataToLayer : (context, payload) => {
            context.commit('addDataToLayer', payload)
        },
        incrementNumElements : (context) =>{
            context.commit('incrementNumElements');
        },
        decrementNumElements : (context) =>{
            context.commit('decrementNumElements');
        },
        addArrow : (context, payload) =>{
            context.commit('addArrow', payload);
        },
        updateArrows : (context, payload) =>{
            context.commit('updateArrows', payload);
        },
        deleteArrow : (context, payload) =>{
            context.commit('deleteArrow', payload);
        },
        deleteArrow2 : (context, payload) =>{
            context.commit('deleteArrow2', payload);
        },
        deleteClink : (context, payload) =>{
            context.commit('deleteClink', payload);
        },
        rotateSource : (context, payload) =>{
            context.commit('rotateSource', payload);
        },
        rotateTarget : (context, payload) =>{
            context.commit('rotateTarget', payload);
        },
        hideNodes : (context, payload) =>{
            context.commit ('hideNodes', payload)
        },
        showAllNodes : (context) =>{
            context.commit ('showAllNodes')
        },
        updateTexts : (context, payload) =>{
            context.commit('updateTexts', payload);
        },
        updateArrowLabel : (context, payload) =>{
            context.commit('updateArrowLabel', payload)
        },
        setSelection : (context, payload) =>{
            context.commit('setSelection', payload)
        },
        restartClinks : (context) =>{
            context.commit('restartClinks')
        },
        restartStage : (context) =>{
            context.commit('restartStage')
        },
        setUser : (context, payload) =>{
            context.commit('setUser', payload)
        },
        
    }



})
