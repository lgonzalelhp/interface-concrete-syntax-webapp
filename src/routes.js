import firebase from 'firebase';
import Vue from 'vue';
import Router from 'vue-router';

import Wellcome from './components/Wellcome.vue'
import ModelEditor from './components/ModelEditor.vue'
import ShareMetamodel from './components/ShareMetamodel.vue'
import Login from './components/Login.vue'

Vue.use(Router);



const router = new Router({
    routes: [
        { 
            path: '*',
            redirect: '/login', meta : { title: 'Concrete Syntax WebApp'}
        },
        {   path: '/',
            redirect: '/login', meta : { title: 'Concrete Syntax WebApp'}
        },
        {
            path: '/login',
            name: 'Login',
            component: Login, meta : { title: 'Concrete Syntax WebApp'}
    },
    {path: '/Wellcome', component: Wellcome, meta: {
        title: 'Concrete Syntax WebApp',
        requiresAuth: true
} },
    { path: '/ModelEditor', component: ModelEditor, meta: {
        title: 'Concrete Syntax WebApp', requiresAuth: true
} },
    {path: '/ShareMetamodel', component: ShareMetamodel, meta: {
        title: 'Concrete Syntax WebApp', requiresAuth: true
} }
   
],
mode : 'history'
});

router.beforeEach((to, from, next) => {
    const currentUser = firebase.auth().currentUser;
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    document.title = to.meta.title
    if (requiresAuth && !currentUser) next('login');
    else if (!requiresAuth && currentUser) next('wellcome');
    else next();
  });
  
  export default router;
